package objects;
import java.util.Random;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Vontari,Akshay Reddy
 */
public class StringsAndNumbers {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        /*1.a.	Declaring and initializing the variables as String data types*/
        
        String string1 = "   Welcome";
        String string2 = "   to  ";
        String string3 = "Computer";
        String string4 = " Science     ";
        String string5 = " and ";
        String string6 = "  Information    ";
        String string7 = "Systems   ";
        /*1.b. Concatenating the above strings and stored in variable string8*/
        String string8 = new String(string1.concat(string2).concat(string3).concat(string4).concat(string5).concat(string6).concat(string7));
        /*1.c. Length of concatenated string*/
        System.out.println("The length of the concatenated string is:"+string8.length());
        /*1.d.	Removing the leading and trailing spaces in string4 & stored the resultant String to a new variable named as string9*/
        string4.trim();
        String string9 = new String(string4.trim());
        System.out.println("Length of the trimmed string is:"+string9.length());
        /*Extracting "Science" word from the string8*/
        string8.indexOf("Science");
        /*index value of Science in string8 stored in start variable of int data type which is index of first letter in Science word*/
        int start = string8.indexOf("Science");
        /*adding index of "Science" and length of the word "Science" gives the index value of last letter in the word "Science"*/
        int end = string8.indexOf("Science")+"Science".length();
        /*1.e. using Substring method "Science" word is extracted and index of first e in word is printed*/
        string8.substring(start,end); 
        System.out.println("Index of first e in science is:"+string8.substring(start,end).indexOf("e"));
        
        /*qustion no :02. */
        /* string "rnururrunngisnnurun" stored in s1*/
        String s1 = new String("rnururrunngisnnurun");
       
        System.out.println("First occurrence of word run is:"+s1.indexOf("run"));
        int start1 = s1.indexOf("run");
        int end1 = s1.indexOf("run")+"run".length();
        /*extracting the word "run" using substring method*/
        s1.substring(start1, end1);
        int start2 = s1.indexOf("is");
        int end2 = s1.indexOf("is")+"is".length();
        /*extracting the word "is" using substring method*/
        s1.substring(start2, end2);
        /*concatenating the words gives "runisfun"*/
        System.out.println(s1.substring(start1, end1) + s1.substring(start2, end2)+"fun");
        
        /*3.a.i. Declaring and initializing two variables with int data type*/
        int myValue1 = 4;
        int myValue2 = 6;
        /*printed the result using Math.pow(a,b)*/
        System.out.println(Math.pow(myValue1,myValue2));
        
        /*3.a.ii. Declaring and initializing a variable with double data type and finding squareroot,ceil and floor using Math methods */
        double myNumber = 26.30;
        System.out.println("Square root of the number is:"+Math.sqrt(myNumber));
        System.out.println("Ceil Value is:"+Math.ceil(myNumber));
        System.out.println("Floor Value is:"+Math.floor(myNumber));
        
        /*3.a.iii. Declaring and initializing two variables and finding sin and tan round of values*/
        double myNumber1 = 30,myNumber2 = 75;
        System.out.println(Math.round(Math.sin(myNumber1)));
        System.out.println(Math.round(Math.sin(myNumber2)));
        System.out.println( Math.round(Math.tan(myNumber1)));
        System.out.println( Math.round(Math.tan(myNumber2)));
        
        /*3.b. finding the ceil value of given sinh function*/
       
        System.out.println(Math.ceil(Math.sinh(Math.sqrt(Math.pow(5, 2)+(4*(3*3))+2)/(3 * 2)))); 
        System.out.println("");
        
        
        /*question no:04*/
        /*4.a. with seed value = 10L*/
        /*r1 object is created in random class with seed value 10L which long data type*/
        Random r1 = new Random(10L);
        
        System.out.println("First Random value:"+r1.nextInt(200));
        System.out.println("Second Random value:"+r1.nextInt(200));
        System.out.println("Third Random value:"+r1.nextInt(200));
        System.out.println("Fourth Random value:"+r1.nextInt(200));
        System.out.println("Fifth Random value:"+r1.nextInt(200));
        System.out.println("Sixth Random value:"+r1.nextInt(200));
        System.out.println("Seventh Random value:"+r1.nextInt(200));
        
         /*4.b*/
        System.out.println("\nOn using the number 10L as the seed value in the program i noticed that the seven random values between \n 0(inclusive)to 200(exclusive) printed same result when i run the program two or three times. \n");
        
        /*4.c. without seed value*/
        /*r2 object is created in random class*/
        Random r2 = new Random();
        
        System.out.println("First Random value:"+r2.nextInt(200));
        System.out.println("Second Random value:"+r2.nextInt(200));
        System.out.println("Third Random value:"+r2.nextInt(200));
        System.out.println("Fourth Random value:"+r2.nextInt(200));
        System.out.println("Fifth Random value:"+r2.nextInt(200));
        System.out.println("Sixth Random value:"+r2.nextInt(200));
        System.out.println("Seventh Random value:"+r2.nextInt(200));
         
        
        System.out.println("\nCreating the random class without seed value in the program i noticed that seven random values between \n0(inclusive) to 200(exclusive) printed different result each time when i run the program.\n");
        
       
        /*4.d comparison of 4.b and 4.c results and explanation*/
        System.out.println("On comparing the results of 4.(b) and 4.(c) i noticed that on giving seed value we get same random values\nwhen we run for two or more times.contrastly,without seed value random class gives an output of different\nrandom values when we run the program for two or more times. ");
       
        
        
       
    
        
        
      
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
    }
}
    

